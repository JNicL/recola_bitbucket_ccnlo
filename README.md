# RECOLA README #

* Version 1.2

* * *

[**Recola**][1] is a Fortran95 computer program for the automated
generation and numerical computation of EW and QCD amplitudes in the Standard
Model at next-to-leading order.

* * *

## Features

+ Amplitudes in the 't Hooft-Feynman gauge

+ Complex-mass scheme for unstable particles implemented

+ Possibility of consistent isolation of resonant contributions and of applying the pole approximation

+ Dimensional regularization for ultraviolet and infrared singularities, with the possibility of treating collinear and soft singularities in mass regularization

+ Various renormalization schemes supported for the electromagnetic coupling constant

+ Dynamical Nf-flavour renormalization scheme for the strong coupling constant

+ Computation of next-to-leading-order amplitudes for all helicities and colour structures

+ Computation of next-to-leading-order squared amplitudes summed/averaged over spin and colour

+ Computation of colour- and/or spin-correlated leading-order squared amplitudes for dipole subtraction


## Package content

The RECOLA package contains the following files and folders:

  1. File `recola-X.Y.Z/CMakeLists.txt`:
     CMake makefile to produce Recola Makefile

  2. Folder `recola-X.Y.Z/build`: 
     Build directory, where CMake puts all necessary files for the creation 
     of the library

  3. Folder `recola-X.Y.Z/src`:
      Recola source directory, containing:
      - the source files `input_rcl.f90`, `process_definition_rcl.f90`, 
        `process_generation_rcl.f90`, `process_computation_rcl.f90`, 
        `reset_rcl.f90` with the global variables and public subroutines 
        accessible to the user; 
      - the subdirectory `recola-X.Y.Z/src/internal` with private source 
        files which should not be modified by the user.

  4. Folder `recola-X.Y.Z/demos`: 
      Directory with demo programs illustrating the use of Recola 
      (`demo0_rcl.f90`, `demo1_rcl.f90`, `demo2_rcl.f90`, `demo3_rcl.f90`) 
      and with shell scripts for their compilation and execution (`run`, 
      `draw-tex`).

* * *

## Installation

### Summary of set up

In order to install the Recola library you need a Fortran compiler (gfortran,
ifort, ...), the [CMake][3] build system and the [COLLIER][2] package.  Change
to the subdirectory `recola-X.Y.Z/build` and execute there the shell command
`cmake [options] ..`, followed by `make`:
   
    cd recola_X.Y.Z/build
    cmake [options] ..
    make

If no options are specified, CMake automatically searches for installed 
Fortran compilers and chooses a suited one. 

### Options

The user can force CMake to use a specific compiler by adding in the cmake
command line the option:

    -D CMAKE_Fortran_COMPILER=<comp>
   
where `<comp>` can be `gfortran`, `ifort`, `pgf95`, ... or the full 
path to a compiler.
   
By default, the installation sequence generates the shared library
`librecola.so` in the directory `recola-X.Y.Z`, with the corresponding module
files placed in the subdirectory `recola-X.Y.Z/modules`. 
The option:
   
    -D static=ON
   
causes CMake to create the static library `librecola.a` instead of the shared
one.
   
The installation procedure further links Recola with the Collier library. If not
specified otherwise, CMake assumes the existence of a folder named `COLLIER`
that is located in the parent directory of `recola-X.Y.Z` and that contains the
Collier library `libcollier.so` and/or `libcollier.a`, as well as the
subdirectory `modules` with the module files of Collier. Note that, depending on
whether Recola shall be generated as shared or as static library, the Collier
library must be present in the same format. 
While the location of the `libcollier.so`/`libcollier.a` and the module files
within the Collier folder must be kept as described above, the overall path may
deviate from the default setting. In this case, the full path to the Collier
directory must be given to CMake via the option:

    -D collier_path=<path to Collier>

where `<path to Collier>` can be either an absolute or a relative path.
Moreover, by adding the option:

    -D cmake_collier=ON

to the `cmake` command line, the user can enforce that Collier is (re-)compiled
when the installation sequence for Recola is performed In this case the CMake
makefile of Recola calls the CMake makefile of Collier and generates the Collier
Makefile (any existing Collier Makefile is overwritten).  The subsequent
execution of `make` in `recola-X.Y.Z/build` then generates the Collier library
and module files (placed in the respective directory) in addition to Recola
library and modules.

* * *

## Running demo programs

To create executables for the demo programs of Recola in the directory 
`recola-X.Y.Z/demos`, the command:

    make <demofile>

should be issued in the directory `recola-X.Y.Z/build`, with `<demofile>` 
being either `demo0_rcl`, `demo1_rcl`, `demo2_rcl` or `demo3_rcl`.
Alternatively, the user can execute (after issuing `cmake`) the shell 
scripts `run` with the command:

    ./run <demofile>

in the directory `recola-X.Y.Z/demos`. This generates and runs the respective
executable `<demofile>`. 

The `recola-X.Y.Z/demos` directory also contains the shell script `draw-tex`
which compiles all LaTeX files of the form `process_*.tex` present in the folder
and creates the corresponding `.pdf` files. It can be run executing:

    ./draw-tex

in the directory `recola-X.Y.Z/demos`. 

* * *

## General instruction when using Recola in a Fortran program

In order to use Recola in a Fortran program, its modules have to be 
loaded by including the line:

    use recola

in the preamble of the respective code, and the library `librecola.so` or 
`librecola.a` has to be supplied to the linker. This gives access to the 
public functions and subroutines of the Recola library. The names of all 
these routines end with the suffix `_rcl`. 

Typically, an application of Recola involves the following five steps:

   - **Step 1**: *Setting input parameters (optional)*
   
     The input needed for the computation of SM processes can be set by the 
     user in two ways: either by editing the file `input.f90`, changing 
     there the values of the corresponding variables explicitly, or by 
     making use of subroutines provided by Recola for this purpose.
     Input variables and subroutines are described in the file `input.f90` 
     and in the manual. Since Recola provides default values for all input 
     parameters, this first step is optional.

   - **Step 2**: *Process definition*
   
     Before Recola can be employed to calculate matrix elements for one or 
     more processes, each process must be declared and labelled with a 
     unique identifier. This is done by calling the `define_process_rcl`, 
     as described in the file `process_definition_rcl.f90` and in the 
     manual. 

   - **Step 3**: *Process generation*
   
     In the next step the subroutine `generate_processes_rcl` is called which
     triggers the initialization of the complete list of processes defined in
     step 2. 

   - **Step 4**: *Process computation*
   
     After the arrangements made in the previous steps,
     Recola is ready to calculate amplitudes for any of the processes defined in
     step 2. The computation of the amplitude and of the squared amplitude is
     performed by means of the subroutine `compute_process_rcl`, which uses the
     process-dependent information derived in step 3. The subroutine
     `compute_process_rcl` is called with the momenta of the external particles
     provided by the user.  Recola further provides subroutines that allow to
     obtain particular contributions of the amplitude or the squared amplitude,
     as described in the file `process_computation_rcl.f90` and in the manual.
     Making use of the subroutines `set_alphas_rcl` or
     `compute_running_alphas_rcl` one can also work with a running value for the
     strong coupling constant alpha_s.

   - **Step 5**: *Resetting Recola*
   
     Finally, by calling the subroutine `reset_recola_rcl`, the
     process-dependent information generated in steps 2-4 is deleted and the
     corresponding memory is deallocated. The input variables keep their values
     defined in step 1 before.

Note that these steps have to be followed in the order given above. 
In particular, after step 3 no new process can be defined unless Recola is 
reset (step 5). After step 5 the user can restart with step 1 or step 2. 

* * *

### Dependencies

+ [COLLIER][2] library for evaluating tensor integrals
+ [CMake][3] compilation of **RECOLA**

* * *

### Contact ###

  - Sandro Uccirati <ucciati@to.infn.it>
  - Jean-Nicolas Lang <jlang@physik.uni-wuerzburg.de>
  - Lars Hofer <hofer@ecm.ub.edu>
  - Ansgar Denner <denner@physik.uni-wuerzburg.de>

* * *

[1]: http://recola.hepforge.org/          "RECOLA"
[2]: http://collier.hepforge.org/         "COLLIER""
[3]: https://cmake.org/                   "CMake"
