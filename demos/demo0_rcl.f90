!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  program main_rcl

  use recola

  implicit none

  integer,  parameter :: dp = kind (23d0) ! double precision


    
  real(dp)            :: p(0:3,1:4)

  real(dp)            :: mtop, mu
  real(dp), parameter :: pi = 3.141592653589793238462643d0
  real(dp)            :: one4pi, m, ggttg1l_recola,  normq, normg
  real(dp)            :: cc, cc11, cc12, cc13, cc14, cc23, cc24, cc34

  ! The standard output is selected (switch off)
  one4pi = 0.079577471545947667884441881686257d0
  mtop =173.3d0
  mu = 8000d0


  normq = 2d0*4d0/3d0
  normg = 2d0*3d0
  
  call set_output_file_rcl('*')
  call set_dynamic_settings_rcl(1)
  call set_delta_ir_rcl(0d0,pi**2/12d0)
  call set_print_level_squared_amplitude_rcl(0)
  call set_print_level_correlations_rcl (0)
  !call set_momenta_correction_rcl(.false.)
  call set_pole_mass_top_rcl(mtop,0d0)


  call define_process_rcl(1,'g g -> t t~','NLO')
  call unselect_all_gs_powers_BornAmpl_rcl(1)
  call select_gs_power_BornAmpl_rcl(1,2)
  call unselect_all_gs_powers_LoopAmpl_rcl(1)
  call select_gs_power_LoopAmpl_rcl(1,4)

  call generate_processes_rcl
  

  p(:,1) = [4000d0, 0d0, 0d0, 4000d0]
  p(:,2) = [4000d0, 0d0, 0d0, -4000d0]
  p(:,3) = [4000d0, -208.0123125920907d0, -2835.557270227848d0, -2808.257992967898d0]
  p(:,4) = [4000d0, 208.0123125920907d0, 2835.557270227848d0, 2808.257992967898d0]

  ! LO Color COnservation
  call set_mu_ir_rcl(mu)
  call set_mu_uv_rcl(mu)
  call set_alphas_rcl(one4pi,mu,5)
  call compute_process_rcl(1,p,'LO') 
  call get_squared_amplitude_rcl(1,2,'LO',m)
  
  call compute_all_colour_correlations_rcl(1,p)
  call get_colour_correlation_rcl(1,2,1,2,cc12)
  call get_colour_correlation_rcl(1,2,1,3,cc13)
  call get_colour_correlation_rcl(1,2,1,4,cc14)
  call get_colour_correlation_rcl(1,2,3,4,cc34)

  print*,"Tree -level"
  print*,"2Re<A0|A0>*CA =  2Re<A0|T1.T1|A0> :", m*normg
  print*,"  2Re<A0|(-T1.T2-T1.T3-T1.T4)|A0> :", (-cc12-cc13-cc14)*normg
  print*,""
  print*,"Color conservation:"
  print*," 2Re<A0|(T1.T1+T1.T2+T1.T3+T1.T4)|A0> :", (m+cc12+cc13+cc14)*normg
  print*,""
  print*," T3.T4 = (C1+C2-C3-C4)/2 + T1.T2  (Eq A.7 in hep-ph/9605323)"
  print*,"  2Re<A0|(T3.T4)|A0> :", (cc34)*normq
  print*,"  2Re<A0|((C1+C2-C3-C4)/2 + T1.T2)|A0> :", ((normg-normq)*m + cc12*normg)
  print*,""
  print*,""
  
  call set_mu_ir_rcl(mu)
  call set_mu_uv_rcl(mu)
  call set_alphas_rcl(one4pi,mu,5)
  call compute_process_rcl(1,p,'NLO') 
  call get_squared_amplitude_rcl(1,3,'NLO',m)
  
  call compute_all_colour_correlations_int_rcl(1,p)
  call get_colour_correlation_int_rcl(1,3,1,2,cc12)
  call get_colour_correlation_int_rcl(1,3,1,3,cc13)
  call get_colour_correlation_int_rcl(1,3,1,4,cc14)
  call get_colour_correlation_int_rcl(1,3,3,4,cc34)

  print*,""
  print*,""
  print*,"One-Loop Wrong factor of 1/2"
  print*,"2Re<A0|A1>*CA =  2Re<A0|T1.T1|A1> :", m
  print*,"  -2Re<A0|(T1.T2-T1.T3-T1.T4)|A1> :", (-cc12-cc13-cc14)
  print*,""
  print*,"Color conservation:"
  print*," 2Re<A0|(T1.T1+T1.T2+T1.T3+T1.T4)|A1> :", (2*m+cc12+cc13+cc14)
  print*,""
  print*," T3.T4 = (C1+C2-C3-C4)/2 + T1.T2  (Eq A.7 in hep-ph/9605323)"
  print*,"  2Re<A0|(T3.T4)|A0> :", (cc34)*normq
  print*,"  2Re<A0|((C1+C2-C3-C4)/2 + T1.T2)|A0> :", ((normg-normq)*m*2 + cc12*normg)
  
  call reset_recola_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  end program main_rcl

!#####################################################################
