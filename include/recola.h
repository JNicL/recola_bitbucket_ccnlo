//####################################################################
//
//  File  recola.h
//  is part of RECOLA (REcursive Computation of One Loop Amplitudes)
//
//  Copyright (C) 2015, 2016   Stefano Actis, Ansgar Denner, 
//                             Lars Hofer, Jean-Nicolas Lang, 
//                             Andreas Scharf, Sandro Uccirati
//  
//  C++ interface developed by Benedikt Biedermann and Mathieu Pellen
//
//  RECOLA is licenced under the GNU GPL version 3, 
//         see COPYING for details.
//
//####################################################################

#ifndef RECOLA_H_
#define RECOLA_H_

#include <complex>
struct dcomplex{double dr,di;};

#ifdef __cplusplus
extern "C" {
#endif

  // module input_rcl
  void __input_rcl_MOD_set_pole_mass_z_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_onshell_mass_z_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_pole_mass_w_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_onshell_mass_w_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_pole_mass_h_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_pole_mass_electron_rcl
       (const double*);
  void __input_rcl_MOD_set_pole_mass_muon_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_pole_mass_tau_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_pole_mass_up_rcl
       (const double*);
  void __input_rcl_MOD_set_pole_mass_down_rcl
       (const double*);
  void __input_rcl_MOD_set_pole_mass_charm_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_pole_mass_strange_rcl
       (const double*);
  void __input_rcl_MOD_set_pole_mass_top_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_pole_mass_bottom_rcl
       (const double*,const double*);
  void __input_rcl_MOD_set_light_fermions_rcl
       (const double*);
  void __input_rcl_MOD_set_light_electron_rcl
       ();
  void __input_rcl_MOD_set_light_muon_rcl
       ();
  void __input_rcl_MOD_set_light_tau_rcl
       ();
  void __input_rcl_MOD_set_light_up_rcl
       ();
  void __input_rcl_MOD_set_light_down_rcl
       ();
  void __input_rcl_MOD_set_light_charm_rcl
       ();
  void __input_rcl_MOD_set_light_strange_rcl
       ();
  void __input_rcl_MOD_set_light_top_rcl
       ();
  void __input_rcl_MOD_set_light_bottom_rcl
       ();
  void __input_rcl_MOD_unset_light_electron_rcl
       ();
  void __input_rcl_MOD_unset_light_muon_rcl
       ();
  void __input_rcl_MOD_unset_light_tau_rcl
       ();
  void __input_rcl_MOD_unset_light_up_rcl
       ();
  void __input_rcl_MOD_unset_light_down_rcl
       ();
  void __input_rcl_MOD_unset_light_charm_rcl
       ();
  void __input_rcl_MOD_unset_light_strange_rcl
       ();
  void __input_rcl_MOD_unset_light_top_rcl
       ();
  void __input_rcl_MOD_unset_light_bottom_rcl
       ();
  void __input_rcl_MOD_use_dim_reg_soft_rcl
       ();
  void __input_rcl_MOD_use_mass_reg_soft_rcl
       (const double*);
  void __input_rcl_MOD_set_mass_reg_soft_rcl
       (const double*);
  void __input_rcl_MOD_set_delta_uv_rcl
       (const double*);
  void __input_rcl_MOD_set_mu_uv_rcl
       (const double*);
  void __input_rcl_MOD_set_delta_ir_rcl
       (const double*, const double*);
  void __input_rcl_MOD_set_mu_ir_rcl
       (const double*);
  void __input_rcl_MOD_set_alphas_rcl
       (const double*,const double*,const int*);
  void __input_rcl_MOD_get_alphas_rcl
       (double*);
  void __input_rcl_MOD_get_renormalization_scale_rcl
       (double*);
  void __input_rcl_MOD_get_flavour_scheme_rcl
       (int*);
  void __input_rcl_MOD_set_alphas_masses_rcl
       (const double*,const double*,const double*,
        const double*,const double*,const double*);
  void __wrapper_rcl_MOD_set_alphas_masses_nowidtharg_rcl
       (const double*,const double*,const double*);
  void __wrapper_rcl_MOD_use_gfermi_scheme_noarg_rcl
       ();
  void __wrapper_rcl_MOD_use_gfermi_scheme_and_set_alpha_rcl
       (const double*);
  void __wrapper_rcl_MOD_use_gfermi_scheme_and_set_gfermi_rcl
       (const double*);
  void __input_rcl_MOD_use_alpha0_scheme_rcl
       (const double*);
  void __wrapper_rcl_MOD_use_alpha0_scheme_noarg_rcl
       ();
  void __input_rcl_MOD_use_alphaz_scheme_rcl
       (const double*);
  void __wrapper_rcl_MOD_use_alphaz_scheme_noarg_rcl
       ();
  void __input_rcl_MOD_get_alpha_rcl
       (double*);
  void __input_rcl_MOD_set_complex_mass_scheme_rcl
       ();
  void __input_rcl_MOD_set_on_shell_scheme_rcl
       ();
  void __input_rcl_MOD_set_resonant_particle_rcl
       (const char*,long int);
  void __input_rcl_MOD_switchon_resonant_selfenergies_rcl
       ();
  void __input_rcl_MOD_switchoff_resonant_selfenergies_rcl
       ();
  void __input_rcl_MOD_set_dynamic_settings_rcl
       (const int*);
  void __input_rcl_MOD_set_momenta_correction_rcl
       (const int*);
  void __input_rcl_MOD_set_draw_level_branches_rcl
       (const int*);
  void __input_rcl_MOD_set_print_level_amplitude_rcl
       (const int*);
  void __input_rcl_MOD_set_print_level_squared_amplitude_rcl
       (const int*);
  void __input_rcl_MOD_set_print_level_correlations_rcl
       (const int*);
  void __input_rcl_MOD_set_print_level_ram_rcl
       (const int*);
  void __input_rcl_MOD_scale_coupling3_rcl
       (dcomplex*,const char*,const char*, const char*,
        long int,long int,long int);
  void __input_rcl_MOD_scale_coupling4_rcl
       (dcomplex*, const char*, const char*, const char*,const char*,
        long int,long int,long int,long int);
  void __input_rcl_MOD_switchoff_coupling3_rcl
       (const char*,const char*,const char*,long int,long int,long int);
  void __input_rcl_MOD_switchoff_coupling4_rcl
       (const char*,const char*,const char*,const char*,
        long int,long int,long int,long int);
  void __input_rcl_MOD_set_ifail_rcl
       (const int*);
  void __input_rcl_MOD_get_ifail_rcl
       (int*);
  void __input_rcl_MOD_set_output_file_rcl
       (const char*,long int);

  // module process_definition_rcl
  void __process_definition_rcl_MOD_define_process_rcl
       (const int*,const char*,const char*,long int,long int);
  void __wrapper_rcl_MOD_wrapper_set_gs_power_rcl
       (const int*, const int[][2], const int*);
  void __process_definition_rcl_MOD_select_gs_power_bornampl_rcl
       (const int*, const int*);
  void __process_definition_rcl_MOD_select_gs_power_loopampl_rcl
       (const int*, const int*);
  void __process_definition_rcl_MOD_unselect_gs_power_bornampl_rcl
       (const int*, const int*);
  void __process_definition_rcl_MOD_unselect_gs_power_loopampl_rcl
       (const int*, const int*);
  void __process_definition_rcl_MOD_select_all_gs_powers_bornampl_rcl
       (const int*);
  void __process_definition_rcl_MOD_select_all_gs_powers_loopampl_rcl
       (const int*);
  void __process_definition_rcl_MOD_unselect_all_gs_powers_bornampl_rcl
       (const int*);
  void __process_definition_rcl_MOD_unselect_all_gs_powers_loopampl_rcl
       (const int*);
  void __process_definition_rcl_MOD_set_collier_cache_rcl
       (const int*,const int*);

  // module process_generation_rcl
  void __process_generation_rcl_MOD_generate_processes_rcl
       ();

  // module process_computation_rcl
  void __process_computation_rcl_MOD_set_resonant_squared_momentum_rcl
       (const int*, const int*, const double*);
  void __process_computation_rcl_MOD_compute_running_alphas_rcl
       (const double*, const int*, const int*);
  void __wrapper_rcl_MOD_wrapper_compute_process_rcl
       (const int*, const double[][4], const int*, 
        const char*, double*, int*, long int);
  void __wrapper_rcl_MOD_wrapper_rescale_process_rcl
       (const int*, const char*, double*, long int);

  // get_colour_configurations_rcl

  // get_helicity_configurations_rcl

  void __wrapper_rcl_MOD_wrapper_get_amplitude_rcl
       (const int*, const int*, const char*, const int[], const int[], 
        const int*, dcomplex*, long int);
  void __process_computation_rcl_MOD_get_squared_amplitude_rcl
       (const int*, const int*, const char*, double*, long int);
  void __wrapper_rcl_MOD_wrapper_get_polarized_squared_amplitude_rcl
       (const int*, const int*, const char*, const int[], 
        const int*, double*, long int);
  void __wrapper_rcl_MOD_wrapper_compute_colour_correlation_rcl
       (const int*, const double[][4], const int*, const int*, 
        const int*, double*, int*);
  void __wrapper_rcl_MOD_wrapper_compute_all_colour_correlations_rcl
       (const int*, const double[][4], const int*, int*);
  void __wrapper_rcl_MOD_wrapper_rescale_colour_correlation_rcl
       (const int*, const int*, const int*, double*);
  void __process_computation_rcl_MOD_rescale_all_colour_correlations_rcl
       (const int*);
  void __process_computation_rcl_MOD_get_colour_correlation_rcl
       (const int*, const int*, const int*, const int*, double*);
  void __wrapper_rcl_MOD_wrapper_compute_spin_colour_correlation_rcl
       (const int*, const double [][4], const int*, const int*, const int*,
        const dcomplex[4], double*, int*);
  void __wrapper_rcl_MOD_wrapper_rescale_spin_colour_correlation_rcl
       (const int*, const int*, const int*,const dcomplex[4], double*);
  void __process_computation_rcl_MOD_get_spin_colour_correlation_rcl
       (const int*, const int*, const int*, const int*, double*);
  void __wrapper_rcl_MOD_wrapper_compute_spin_correlation_rcl
       (const int*, const double[][4], const int*, const int*, 
        const dcomplex[4], double*, int*);
  void __wrapper_rcl_MOD_wrapper_rescale_spin_correlation_rcl
       (const int*, const int*, const dcomplex[4], double*);
  void __process_computation_rcl_MOD_get_spin_correlation_rcl
       (const int*, const int*, double*);
  void __wrapper_rcl_MOD_get_legs_rcl
       (const int*, int*);
  void __wrapper_rcl_MOD_wrapper_get_momenta_rcl
       (const int*, double [][4], const int*);
  void __process_computation_MOD_set_tis_required_accuracy_rcl
       (const double*);
  void __process_computation_MOD_get_tis_required_accuracy_rcl
       (double*);
  void __process_computation_MOD_set_tis_critical_accuracy_rcl
       (const double*);
  void __process_computation_MOD_get_tis_critical_accuracy_rcl
       (double*);
  void __process_computation_MOD_get_tis_accuracy_flag_rcl
       (int*);

  // module reset_rcl
  void __reset_rcl_MOD_reset_recola_rcl
       ();

#ifdef __cplusplus
}
#endif

namespace Recola{
/*
  module input_rcl
*/
inline void set_pole_mass_z_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_pole_mass_z_rcl(&m,&g);
}
inline void set_onshell_mass_z_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_onshell_mass_z_rcl(&m,&g);
}
inline void set_pole_mass_w_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_pole_mass_w_rcl(&m,&g);
}
inline void set_onshell_mass_w_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_onshell_mass_w_rcl(&m,&g);
}
inline void set_pole_mass_h_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_pole_mass_h_rcl(&m,&g);
}
inline void set_pole_mass_electron_rcl
            (const double m)
{
  __input_rcl_MOD_set_pole_mass_electron_rcl(&m);
}
inline void set_pole_mass_muon_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_pole_mass_muon_rcl(&m,&g);
}
inline void set_pole_mass_tau_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_pole_mass_tau_rcl(&m,&g);
}
inline void set_pole_mass_up_rcl
            (const double m)
{
  __input_rcl_MOD_set_pole_mass_up_rcl(&m);
}
inline void set_pole_mass_down_rcl
            (const double m)
{
  __input_rcl_MOD_set_pole_mass_down_rcl(&m);
}
inline void set_pole_mass_charm_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_pole_mass_charm_rcl(&m,&g);
}
inline void set_pole_mass_strange_rcl
            (const double m)
{
  __input_rcl_MOD_set_pole_mass_strange_rcl(&m);
}
inline void set_pole_mass_top_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_pole_mass_top_rcl(&m,&g);
}
inline void set_pole_mass_bottom_rcl
            (const double m, const double g)
{
  __input_rcl_MOD_set_pole_mass_bottom_rcl(&m,&g);
}
inline void set_light_fermions_rcl
            (const double m)
{
  __input_rcl_MOD_set_light_fermions_rcl(&m);
}
inline void set_light_electron_rcl()
{
  __input_rcl_MOD_set_light_electron_rcl();
}
inline void set_light_muon_rcl()
{
  __input_rcl_MOD_set_light_muon_rcl();
}
inline void set_light_tau_rcl()
{
  __input_rcl_MOD_set_light_tau_rcl();
}
inline void set_light_up_rcl()
{
  __input_rcl_MOD_set_light_up_rcl();
}
inline void set_light_down_rcl()
{
  __input_rcl_MOD_set_light_down_rcl();
}
inline void set_light_charm_rcl()
{
  __input_rcl_MOD_set_light_charm_rcl();
}
inline void set_light_strange_rcl()
{
  __input_rcl_MOD_set_light_strange_rcl();
}
inline void set_light_top_rcl()
{
  __input_rcl_MOD_set_light_top_rcl();
}
inline void set_light_bottom_rcl()
{
  __input_rcl_MOD_set_light_bottom_rcl();
}
inline void unset_light_electron_rcl()
{
  __input_rcl_MOD_unset_light_electron_rcl();
}
inline void unset_light_muon_rcl()
{
  __input_rcl_MOD_unset_light_muon_rcl();
}
inline void unset_light_tau_rcl()
{
  __input_rcl_MOD_unset_light_tau_rcl();
}
inline void unset_light_up_rcl()
{
  __input_rcl_MOD_unset_light_up_rcl();
}
inline void unset_light_down_rcl()
{
  __input_rcl_MOD_unset_light_down_rcl();
}
inline void unset_light_charm_rcl()
{
  __input_rcl_MOD_unset_light_charm_rcl();
}
inline void unset_light_strange_rcl()
{
  __input_rcl_MOD_unset_light_strange_rcl();
}
inline void unset_light_top_rcl()
{
  __input_rcl_MOD_unset_light_top_rcl();
}
inline void unset_light_bottom_rcl()
{
  __input_rcl_MOD_unset_light_bottom_rcl();
}
inline void use_dim_reg_soft_rcl()
{
  __input_rcl_MOD_use_dim_reg_soft_rcl();
}
inline void use_mass_reg_soft_rcl
            (const double m)
{
  __input_rcl_MOD_use_mass_reg_soft_rcl(&m);
}
inline void set_mass_reg_soft_rcl
            (const double m)
{
  __input_rcl_MOD_set_mass_reg_soft_rcl(&m);
}
inline void set_delta_uv_rcl
            (const double d)
{
  __input_rcl_MOD_set_delta_uv_rcl(&d);
}
inline void set_mu_uv_rcl
            (const double m)
{
  __input_rcl_MOD_set_mu_uv_rcl(&m);
}
inline void set_delta_ir_rcl
            (const double d1, const double d2)
{
  __input_rcl_MOD_set_delta_ir_rcl(&d1,&d2);
}
inline void set_mu_ir_rcl
            (const double m)
{
  __input_rcl_MOD_set_mu_ir_rcl(&m);
}
inline void set_alphas_rcl
            (const double a, const double s, const int nf)
{
  __input_rcl_MOD_set_alphas_rcl(&a,&s,&nf);
}
inline void get_alphas_rcl
            (double &a)
{
  __input_rcl_MOD_get_alphas_rcl(&a);
}
inline void get_renormalization_scale_rcl
            (double &mu)
{
  __input_rcl_MOD_get_renormalization_scale_rcl(&mu);
}
inline void get_flavour_scheme_rcl
            (int &nf)
{
  __input_rcl_MOD_get_flavour_scheme_rcl(&nf);
}
inline void set_alphas_masses_rcl
            (const double mc, const double mb, const double mt,
	           const double wc, const double wb, const double wt)
{
  __input_rcl_MOD_set_alphas_masses_rcl(&mc,&mb,&mt,&wc,&wb,&wt);
}
inline void set_alphas_masses_rcl
             (const double mc, const double mb, const double mt)
{
  __wrapper_rcl_MOD_set_alphas_masses_nowidtharg_rcl(&mc,&mb,&mt);
} 
inline void use_gfermi_scheme_rcl()
{
  __wrapper_rcl_MOD_use_gfermi_scheme_noarg_rcl();
}
inline void use_gfermi_scheme_and_set_alpha_rcl
            (const double a)
{
  __wrapper_rcl_MOD_use_gfermi_scheme_and_set_alpha_rcl(&a);
}
inline void use_gfermi_scheme_and_set_gfermi_rcl
            (const double g)
{
  __wrapper_rcl_MOD_use_gfermi_scheme_and_set_gfermi_rcl(&g);
}
inline void use_alpha0_scheme_rcl
            (const double a)
{
  __input_rcl_MOD_use_alpha0_scheme_rcl(&a);
}
inline void use_alpha0_scheme_rcl()
{
  __wrapper_rcl_MOD_use_alpha0_scheme_noarg_rcl();
}
inline void use_alphaz_scheme_rcl
            (const double a)
{
  __input_rcl_MOD_use_alphaz_scheme_rcl(&a);
}
inline void use_alphaz_scheme_rcl()
{
  __wrapper_rcl_MOD_use_alphaz_scheme_noarg_rcl();
}
inline void get_alpha_rcl
            (double &a)
{
  __input_rcl_MOD_get_alpha_rcl(&a);
}
inline void set_complex_mass_scheme_rcl()
{
  __input_rcl_MOD_set_complex_mass_scheme_rcl();
}
inline void set_on_shell_scheme_rcl()
{
  __input_rcl_MOD_set_on_shell_scheme_rcl();
}
inline void set_resonant_particle_rcl
            (const std::string pa)
{
  __input_rcl_MOD_set_resonant_particle_rcl(pa.c_str(),pa.length());
}
inline void switchon_resonant_selfenergies_rcl()
{
  __input_rcl_MOD_switchon_resonant_selfenergies_rcl();
}
inline void switchoff_resonant_selfenergies_rcl()
{
  __input_rcl_MOD_switchoff_resonant_selfenergies_rcl();
}
inline void set_dynamic_settings_rcl
            (const int n)
{
  __input_rcl_MOD_set_dynamic_settings_rcl(&n);
}
inline void set_momenta_correction_rcl
            (const bool mc)
{
  int boolint;
  if (mc) {
    boolint = 1;
  } else {
    boolint = 0;
  }
  __input_rcl_MOD_set_momenta_correction_rcl(&boolint);
}
inline void set_draw_level_branches_rcl
            (const int n)
{
  __input_rcl_MOD_set_draw_level_branches_rcl(&n);
}
inline void set_print_level_amplitude_rcl
            (const int n)
{
  __input_rcl_MOD_set_print_level_amplitude_rcl(&n);
}
inline void set_print_level_squared_amplitude_rcl
            (const int n)
{
  __input_rcl_MOD_set_print_level_squared_amplitude_rcl(&n);
}
inline void set_print_level_correlations_rcl
            (const int n)
{
  __input_rcl_MOD_set_print_level_correlations_rcl(&n);
}
inline void set_print_level_RAM_rcl
            (const int n)
{
  __input_rcl_MOD_set_print_level_ram_rcl(&n);
}
inline void scale_coupling3_rcl
            (const std::complex<double> fac, 
             const std::string pa1,
             const std::string pa2, 
             const std::string pa3)
{
   dcomplex dfac;
   dfac.dr = fac.real();
   dfac.di = fac.imag();
   __input_rcl_MOD_scale_coupling3_rcl(&dfac,
                                       pa1.c_str(),
                                       pa2.c_str(),
                                       pa3.c_str(),
                                       pa1.length(),
                                       pa2.length(),
                                       pa3.length());
}
inline void scale_coupling4_rcl
            (const std::complex<double> fac, 
             const std::string pa1,
             const std::string pa2, 
             const std::string pa3,
             const std::string pa4)
{
   dcomplex dfac;
   dfac.dr = fac.real();
   dfac.di = fac.imag();
   __input_rcl_MOD_scale_coupling4_rcl(&dfac,
                                       pa1.c_str(),
                                       pa2.c_str(),
                                       pa3.c_str(),
                                       pa4.c_str(),
                                       pa1.length(),
                                       pa2.length(),
                                       pa3.length(),
                                       pa4.length());
}
inline void switchoff_coupling3_rcl
            (const std::string pa1,
             const std::string pa2,
             const std::string pa3)
{
  __input_rcl_MOD_switchoff_coupling3_rcl(pa1.c_str(),
                                          pa2.c_str(),
                                          pa3.c_str(),
                                          pa1.length(),
                                          pa2.length(),
                                          pa3.length());
}
inline void switchoff_coupling4_rcl
            (const std::string pa1,
             const std::string pa2,
             const std::string pa3,
             const std::string pa4)
{
  __input_rcl_MOD_switchoff_coupling4_rcl(pa1.c_str(),
                                          pa2.c_str(),
                                          pa3.c_str(),
                                          pa4.c_str(),
                                          pa1.length(),
                                          pa2.length(),
                                          pa3.length(),
                                          pa4.length());
}
inline void set_ifail_rcl
            (const int i)
{
   __input_rcl_MOD_set_ifail_rcl(&i);
}
inline void get_ifail_rcl
            (int &i)
{
   __input_rcl_MOD_get_ifail_rcl(&i);
}
inline void set_output_file_rcl
            (const std::string x)
{
  __input_rcl_MOD_set_output_file_rcl(x.c_str(),x.length());
}
/*
  module process_defintion_rcl
*/
inline void define_process_rcl
            (const int npr,
             const std::string process, 
             const std::string order)
{
  __process_definition_rcl_MOD_define_process_rcl(&npr,
                                                  process.c_str(),
                                                  order.c_str(),
                                                  process.length(),
                                                  order.length());
}
inline void set_gs_power_rcl
            (const int npr, const int gsarray[][2], const int gslen)
{
   __wrapper_rcl_MOD_wrapper_set_gs_power_rcl(&npr,gsarray,&gslen);
}
inline void select_gs_power_BornAmpl_rcl
            (const int npr, const int gspower)
{
  __process_definition_rcl_MOD_select_gs_power_bornampl_rcl(&npr,&gspower);
}
inline void select_gs_power_LoopAmpl_rcl
            (const int npr, const int gspower)
{
  __process_definition_rcl_MOD_select_gs_power_loopampl_rcl(&npr,&gspower);
}
inline void unselect_gs_power_BornAmpl_rcl
            (const int npr, const int gspower)
{
  __process_definition_rcl_MOD_unselect_gs_power_bornampl_rcl(&npr,&gspower);
}
inline void unselect_gs_power_LoopAmpl_rcl
            (const int npr, const int gspower)
{
  __process_definition_rcl_MOD_unselect_gs_power_loopampl_rcl(&npr,&gspower);
}
inline void select_all_gs_powers_BornAmpl_rcl
            (const int npr)
{
  __process_definition_rcl_MOD_select_all_gs_powers_bornampl_rcl(&npr);
}
inline void select_all_gs_powers_LoopAmpl_rcl
            (const int npr)
{
  __process_definition_rcl_MOD_select_all_gs_powers_loopampl_rcl(&npr);
}
inline void unselect_all_gs_powers_BornAmpl_rcl
            (const int npr)
{
  __process_definition_rcl_MOD_unselect_all_gs_powers_bornampl_rcl(&npr);
}
inline void unselect_all_gs_powers_LoopAmpl_rcl
            (const int npr)
{
  __process_definition_rcl_MOD_unselect_all_gs_powers_loopampl_rcl(&npr);
}
inline void set_collier_cache_rcl(const int npr, const int n)
{
  __process_definition_rcl_MOD_set_collier_cache_rcl(&npr,&n);
}
/*
  module process_generation_rcl:
*/
inline void generate_processes_rcl()
{
  __process_generation_rcl_MOD_generate_processes_rcl();
}
/*
  module process_computation_rcl:
*/
inline void set_resonant_squared_momentum_rcl
            (const int npr,const int res, const double ps)
{
  __process_computation_rcl_MOD_set_resonant_squared_momentum_rcl(&npr,&res,&ps);
}
inline void compute_running_alphas_rcl
            (const double q, const int nf, const int lp)
{
  __process_computation_rcl_MOD_compute_running_alphas_rcl(&q,&nf,&lp);
}

/*compute_process_rcl is overloaded*/
inline void compute_process_rcl
            (const int npr, 
             const double p[][4],
             const std::string order, 
             double A2[2], 
             bool& momenta_check)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_process_rcl(&npr,
                                                p,
                                                &legs,
                                                order.c_str(),
                                                A2,
                                                &boolint,
                                                order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
inline void compute_process_rcl
            (const int npr, 
             const double p[][4],
             const std::string order, 
             bool& momenta_check)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  double A2[2];
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_process_rcl(&npr,
                                                p,
                                                &legs,
                                                order.c_str(),
                                                A2,
                                                &boolint,
                                                order.length());
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
inline void compute_process_rcl
            (const int npr, 
             const double p[][4],
             const std::string order, 
             double A2[2])
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_process_rcl(&npr,
                                                p,
                                                &legs,
                                                order.c_str(),
                                                A2,
                                                &boolint,
                                                order.length());
}
inline void compute_process_rcl
            (const int npr, 
             const double p[][4],
             const std::string order)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  double A2[2];
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_process_rcl(&npr,
                                                p,
                                                &legs,
                                                order.c_str(),
                                                A2,
                                                &boolint,
                                                order.length());
}
inline void rescale_process_rcl
            (const int npr, const std::string order, double A2[2])
{
  __wrapper_rcl_MOD_wrapper_rescale_process_rcl(&npr,
                                                order.c_str(),
                                                A2,
                                                order.length());
}
inline void rescale_process_rcl
            (const int npr, const std::string order)
{
  double A2[2];
  __wrapper_rcl_MOD_wrapper_rescale_process_rcl(&npr,
                                                order.c_str(),
                                                A2,
                                                order.length());
}

  // get_colour_configurations_rcl

  // get_helicity_configurations_rcl

inline void get_amplitude_rcl
            (const int npr, 
             const int pow, 
             const std::string order, 
             const int colour[], const int hel[],
             std::complex<double>& A)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  dcomplex dA;
  dA.di = A.imag();
  dA.dr = A.real();
  __wrapper_rcl_MOD_wrapper_get_amplitude_rcl(&npr,
                                              &pow,
                                              order.c_str(), 
                                              colour,
                                              hel,
                                              &legs,
                                              &dA,
                                              order.length());
  A = std::complex<double> (dA.dr, dA.di);
}
inline void get_squared_amplitude_rcl
            (const int npr, 
             const int pow, 
             const std::string order, 
             double &A2)
{
  __process_computation_rcl_MOD_get_squared_amplitude_rcl(&npr,
                                                          &pow,
                                                          order.c_str(),
                                                          &A2,
                                                          order.length());
}
inline void get_polarized_squared_amplitude_rcl
            (const int npr, 
             const int pow, 
             const std::string order,
             const int hel[], 
             double &A2h)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  __wrapper_rcl_MOD_wrapper_get_polarized_squared_amplitude_rcl(&npr,
                                                                &pow,
                                                                order.c_str(),
                                                                hel,
                                                                &legs,
                                                                &A2h,
                                                                order.length());
}
inline void compute_colour_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int i1, const int i2, 
             double &A2cc, 
             bool& momenta_check)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_colour_correlation_rcl(&npr,
                                                           p,
                                                           &legs,
                                                           &i1,
                                                           &i2,
                                                           &A2cc,
                                                           &boolint);
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
inline void compute_colour_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int i1, const int i2, 
             bool& momenta_check)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  double A2cc;
  __wrapper_rcl_MOD_wrapper_compute_colour_correlation_rcl(&npr,
                                                           p,
                                                           &legs,
                                                           &i1,
                                                           &i2,
                                                           &A2cc,
                                                           &boolint);
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
inline void compute_colour_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int i1, const int i2, 
             double &A2cc)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_colour_correlation_rcl(&npr,
                                                           p,
                                                           &legs,
                                                           &i1,
                                                           &i2,
                                                           &A2cc,
                                                           &boolint);
}
inline void compute_colour_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int i1, const int i2)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  double A2cc;
  __wrapper_rcl_MOD_wrapper_compute_colour_correlation_rcl(&npr,
                                                           p,
                                                           &legs,
                                                           &i1,
                                                           &i2,
                                                           &A2cc,
                                                           &boolint);
}
inline void compute_all_colour_correlations_rcl
            (const int npr, 
             const double p[][4], 
             bool& momenta_check)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_all_colour_correlations_rcl(&npr,
                                                                p,
                                                                &legs,
                                                                &boolint);
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
inline void compute_all_colour_correlations_rcl
            (const int npr, 
             const double p[][4])
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_all_colour_correlations_rcl(&npr,
                                                                p,
                                                                &legs,
                                                                &boolint);
}
inline void rescale_colour_correlation_rcl
            (const int npr, 
             const int i1, const int i2, 
             double &A2cc)
{
  __wrapper_rcl_MOD_wrapper_rescale_colour_correlation_rcl(&npr,
                                                           &i1,
                                                           &i2,
                                                           &A2cc);
}
inline void rescale_colour_correlation_rcl
            (const int npr, 
             const int i1, const int i2)
{
  double A2cc;
  __wrapper_rcl_MOD_wrapper_rescale_colour_correlation_rcl(&npr,
                                                           &i1,
                                                           &i2,
                                                           &A2cc);
}
inline void rescale_all_colour_correlations_rcl
            (const int npr)
{
  __process_computation_rcl_MOD_rescale_all_colour_correlations_rcl(&npr);
}
inline void get_colour_correlation_rcl
            (const int npr, 
             const int pow, 
             const int i1, const int i2, 
             double& A2cc)
{
  __process_computation_rcl_MOD_get_colour_correlation_rcl(&npr,
                                                           &pow,
                                                           &i1,
                                                           &i2,
                                                           &A2cc);
}
inline void compute_spin_colour_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int i1, const int i2, 
             const std::complex<double> v[4], 
             double &A2scc, 
             bool& momenta_check)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_spin_colour_correlation_rcl(&npr,
                                                                p,
                                                                &legs,
                                                                &i1,
                                                                &i2,
                                                                dfac,
                                                                &A2scc,
                                                                &boolint);
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
inline void compute_spin_colour_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int i1, const int i2, 
             const std::complex<double> v[4], 
             bool& momenta_check)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  double A2scc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_spin_colour_correlation_rcl(&npr,
                                                                p,
                                                                &legs,
                                                                &i1,
                                                                &i2,
                                                                dfac,
                                                                &A2scc,
                                                                &boolint);
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
inline void compute_spin_colour_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int i1, const int i2, 
             const std::complex<double> v[4], 
             double &A2scc)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  __wrapper_rcl_MOD_wrapper_compute_spin_colour_correlation_rcl(&npr,
                                                                p,
                                                                &legs,
                                                                &i1,
                                                                &i2,
                                                                dfac,
                                                                &A2scc,
                                                                &boolint);
}
inline void compute_spin_colour_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int i1, const int i2, 
             const std::complex<double> v[4])
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  double A2scc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  __wrapper_rcl_MOD_wrapper_compute_spin_colour_correlation_rcl(&npr,
                                                                p,
                                                                &legs,
                                                                &i1,
                                                                &i2,
                                                                dfac,
                                                                &A2scc,
                                                                &boolint);
}
inline void rescale_spin_colour_correlation_rcl
            (const int npr, 
             const int i1, const int i2,  
             const std::complex<double> v[4], 
             double &A2scc)
{
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  __wrapper_rcl_MOD_wrapper_rescale_spin_colour_correlation_rcl(&npr,
                                                                &i1,
                                                                &i2,
                                                                dfac,
                                                                &A2scc);
}
inline void rescale_spin_colour_correlation_rcl
            (const int npr, 
             const int i1, const int i2,  
             const std::complex<double> v[4])
{
  double A2scc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  __wrapper_rcl_MOD_wrapper_rescale_spin_colour_correlation_rcl(&npr,
                                                                &i1,
                                                                &i2,
                                                                dfac,
                                                                &A2scc);
}
inline void get_spin_colour_correlation_rcl
            (const int npr, 
             const int pow, 
             const int i1, const int i2, 
             double &A2scc)
{
  __process_computation_rcl_MOD_get_spin_colour_correlation_rcl(&npr,
                                                                &pow,
                                                                &i1,
                                                                &i2,
                                                                &A2scc);
}
inline void compute_spin_correlation_rcl
            (const int npr, 
             const double p[][4],
             const int j, 
             const std::complex<double> v[4],
             double &A2sc, 
             bool& momenta_check)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_spin_correlation_rcl(&npr,
                                                         p,
                                                         &legs,
                                                         &j,
                                                         dfac,
                                                         &A2sc,
                                                         &boolint);
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
inline void compute_spin_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int j, 
             const std::complex<double> v[4],
             bool& momenta_check)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  double A2sc; 
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  int boolint;
  __wrapper_rcl_MOD_wrapper_compute_spin_correlation_rcl(&npr,
                                                         p,
                                                         &legs,
                                                         &j,
                                                         dfac,
                                                         &A2sc,
                                                         &boolint);
  if (boolint) {
    momenta_check = true;
  } else {
    momenta_check = false;
  }
}
inline void compute_spin_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int j, 
             const std::complex<double> v[4],
             double &A2sc)
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  __wrapper_rcl_MOD_wrapper_compute_spin_correlation_rcl(&npr,
                                                         p,
                                                         &legs,
                                                         &j,
                                                         dfac,
                                                         &A2sc,
                                                         &boolint);
}
inline void compute_spin_correlation_rcl
            (const int npr, 
             const double p[][4], 
             const int j, 
             const std::complex<double> v[4])
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  int boolint;
  double A2sc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  __wrapper_rcl_MOD_wrapper_compute_spin_correlation_rcl(&npr,
                                                         p,
                                                         &legs,
                                                         &j,
                                                         dfac,
                                                         &A2sc,
                                                         &boolint);
}
inline void rescale_spin_correlation_rcl
            (const int npr, 
             const int j, 
             const std::complex<double> v[4],
             double &A2sc)
{
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  __wrapper_rcl_MOD_wrapper_rescale_spin_correlation_rcl(&npr,
                                                         &j,
                                                         dfac,
                                                         &A2sc);
}
inline void rescale_spin_correlation_rcl
            (const int npr, 
             const int j, 
             const std::complex<double> v[4])
{
  double A2sc;
  dcomplex dfac[4];
  for (int i = 0; i < 4; ++i) {
    dfac[i].dr = v[i].real();
    dfac[i].di = v[i].imag();
  }
  __wrapper_rcl_MOD_wrapper_rescale_spin_correlation_rcl(&npr,
                                                         &j,
                                                         dfac,
                                                         &A2sc);
}
inline void get_spin_correlation_rcl
            (const int npr, const int pow, double &A2sc)
{
  __process_computation_rcl_MOD_get_spin_correlation_rcl(&npr,
                                                         &pow,
                                                         &A2sc);
}
inline void get_momenta_rcl
            (const int npr, double p[][4])
{
  int legs;
  __wrapper_rcl_MOD_get_legs_rcl(&npr, &legs);
  __wrapper_rcl_MOD_wrapper_get_momenta_rcl(&npr,p,&legs);
}
inline void set_tis_required_accuracy_rcl
            (const double acc)
{
  __process_computation_MOD_set_tis_required_accuracy_rcl(&acc);
}
inline void get_tis_required_accuracy_rcl
            (double &acc)
{
  __process_computation_MOD_get_tis_required_accuracy_rcl(&acc);
}
inline void set_tis_critical_accuracy_rcl
            (const double acc)
{
  __process_computation_MOD_set_tis_critical_accuracy_rcl(&acc);
}
inline void get_tis_critical_accuracy_rcl
            (double &acc)
{
  __process_computation_MOD_get_tis_critical_accuracy_rcl(&acc);
}
inline void get_tis_accuracy_flag_rcl
            (int &flag)
{
  __process_computation_MOD_get_tis_accuracy_flag_rcl(&flag);
}
/*
  module reset_rcl
*/

inline void reset_recola_rcl()
{
  __reset_rcl_MOD_reset_recola_rcl();
}

}

#endif // RECOLA_H_
