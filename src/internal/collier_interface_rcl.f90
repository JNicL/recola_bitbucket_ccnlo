!#####################################################################
!!
!!  File  collier_interface_rcl.f90
!!  is part of RECOLA (REcursive Computation of One Loop Amplitudes)
!!
!!  Copyright (C) 2015-2017   Stefano Actis, Ansgar Denner, 
!!                            Lars Hofer, Jean-Nicolas Lang, 
!!                            Andreas Scharf, Sandro Uccirati
!!
!!  RECOLA is licenced under the GNU GPL version 3, 
!!         see COPYING for details.
!!
!#####################################################################

  module collier_interface_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  use input_rcl
  use collier

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  implicit none

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  contains

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine initialize_collier

  integer                  :: pr,nPropMax,i,n
  integer, allocatable     :: nProp(:)
  complex(dp), allocatable :: m2(:)

  allocate (nProp(prTot)); nProp = 0
  do pr = 1,prTot
    if (loop(pr)) nProp(pr) = maxval(legsti(:tiTot(pr),pr))
  enddo
  nPropMax = maxval(nProp)

  call Init_cll(nPropMax,noreset=.true.)

  if (.not.allocated(nCache)) then
    allocate(nCache(1:prTot)); nCache = 1
  endif
  if (.not.allocated(cacheOn)) then
    allocate(cacheOn(1:prTot)); cacheOn = .true.
  endif
  allocate(nCacheTot(0:prTot),tiCache(prTot))

  nCacheTot = 0
  do pr = 1,prTot
    if (loop(pr)) tiCache(pr) = ceiling(tiTot(pr)*1d0/nCache(pr))
    nCacheTot(pr) = nCacheTot(pr-1) + nCache(pr)
!    write(nx,*) 'nCache  =',nCache(pr)
!    write(nx,*) 'tiTot   =',tiTot(pr)
!    write(nx,*) 'tiCache =',tiCache(pr)
!    write(nx,*)
  enddo

  call InitCacheSystem_cll(nCacheTot(prTot),nPropMax)

  call SetMode_cll(1)

  call SwitchOnTenRed_cll
!  call SwitchOffTenRed_cll

  do pr = 1,prTot
    do n = nCacheTot(pr-1)+1,nCacheTot(pr)
      call SetCacheLevel_cll(n,nProp(pr))
      if (.not.cacheOn(pr)) call SwitchOffCache_cll(n)
    enddo
  enddo

  if (ifail.ge.0) call SetErrStop_cll(-11) 

  n = 0
  do i = 23,31
    if (regf(i).eq.2) n = n + 1
  enddo
  allocate (m2(n))
  n = 0
  do i = 23,31
    if (regf(i).eq.2) then
      n = n + 1
      if (complex_mass_scheme.eq.1) then
        m2(n) = cm2n(nmf(i))
      else
        m2(n) = real(cm2n(nmf(i)),kind=dp)*c1d0
      endif
    endif
  enddo
  call setminf2_cll (n,m2)
  deallocate (m2)

  call SetDeltaUV_cll (deltaUV)
  call SetDeltaIR_cll (deltaIR,deltaIR2)

  if (reg_soft.eq.1) then
    call SetMuIR2_cll(muIR**2)
  else
    call SetMuIR2_cll(lambda**2)
  endif
  call SetMuUV2_cll(muUV**2) 

  deallocate(nProp)

  end subroutine initialize_collier

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine initialize_collier_ct

  integer                  :: i,n
  complex(dp), allocatable :: m2(:)

  call Init_cll(2,noreset=.true.)
  call SetMode_cll(1)
  n = 0
  do i = 23,31
    if (regf(i).eq.2) n = n + 1
  enddo
  allocate (m2(n))
  n = 0
  do i = 23,31
    if (regf(i).eq.2) then
      n = n + 1
      if (complex_mass_scheme.eq.1) then
        m2(n) = cm2n(nmf(i))
      else
        m2(n) = real(cm2n(nmf(i)),kind=dp)*c1d0
      endif
    endif
  enddo
  call setminf2_cll (n,m2)
  deallocate (m2)

  call SetDeltaUV_cll (deltaUV)
  call SetDeltaIR_cll (deltaIR,deltaIR2)

  if (reg_soft.eq.1) then
    call SetMuIR2_cll(muIR**2)
  else
    call SetMuIR2_cll(lambda**2)
  endif
  call SetMuUV2_cll(muUV**2) 

  end subroutine initialize_collier_ct

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  end module collier_interface_rcl

!#####################################################################
