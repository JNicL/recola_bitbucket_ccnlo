!#####################################################################
!!
!!  File  wrapper_rcl.f90
!!  is part of RECOLA (REcursive Computation of One Loop Amplitudes)
!!
!!  Copyright (C) 2015-2017   Stefano Actis, Ansgar Denner, 
!!                            Lars Hofer, Jean-Nicolas Lang, 
!!                            Andreas Scharf, Sandro Uccirati
!!
!!  RECOLA is licenced under the GNU GPL version 3, 
!!         see COPYING for details.
!!
!#####################################################################

  module wrapper_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  use input_rcl
  use process_computation_rcl
  use process_definition_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  implicit none

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  contains

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine set_alphas_masses_nowidtharg_rcl (mc,mb,mt)

  real(dp), intent(in) :: mc,mb,mt

  call set_alphas_masses_rcl (mc,mb,mt)

  end subroutine set_alphas_masses_nowidtharg_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine use_gfermi_scheme_noarg_rcl

  call use_gfermi_scheme_rcl

  end subroutine use_gfermi_scheme_noarg_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine use_gfermi_scheme_and_set_alpha_rcl (alpha)

  real(dp), intent(in) :: alpha

  call use_gfermi_scheme_rcl (a=alpha)

  end subroutine use_gfermi_scheme_and_set_alpha_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine use_gfermi_scheme_and_set_gfermi_rcl (gfermi)

  real(dp), intent(in) :: gfermi

  call use_gfermi_scheme_rcl (g=gfermi)

  end subroutine use_gfermi_scheme_and_set_gfermi_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine use_alpha0_scheme_noarg_rcl

  call use_alpha0_scheme_rcl

  end subroutine use_alpha0_scheme_noarg_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine use_alphaz_scheme_noarg_rcl

  call use_alphaz_scheme_rcl

  end subroutine use_alphaz_scheme_noarg_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_set_gs_power_rcl (npr,gsarray,gslen)

  integer, intent(in) :: npr,gslen
  integer, intent(in) :: gsarray(0:1,1:gslen)

  call set_gs_power_rcl(npr,transpose(gsarray))

  end subroutine wrapper_set_gs_power_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_compute_process_rcl (npr,p,legs,order,A2, &
                                          momenta_check)

!  real(dp), intent(out), dimension(0:1), optional :: A2
! we use ALWAYS the A2 argument as mandatory, since the optional form
! conflicts with passing character arrays ("order") from C to Fortran 
! in the same function call

  integer,          intent(in)  :: npr,legs
  real(dp),         intent(in)  :: p(0:3,1:legs)
  character(len=*), intent(in)  :: order
  real(dp),         intent(out) :: A2(0:1)
  logical,          intent(out) :: momenta_check

  call compute_process_rcl (npr,p,order,A2,momenta_check)

  end subroutine wrapper_compute_process_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_rescale_process_rcl (npr,order,A2)

  integer,          intent(in)  :: npr
  character(len=*), intent(in)  :: order
  real(dp),         intent(out) :: A2(0:1)

  call rescale_process_rcl (npr,order,A2)

  end subroutine wrapper_rescale_process_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! get_colour_configurations_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

! get_helicity_configurations_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_get_amplitude_rcl (npr,pow,order,colour,hel,legs,A)

  integer,          intent(in)  :: npr,pow,legs
  integer,          intent(in)  :: colour(1:legs),hel(1:legs)
  character(len=*), intent(in)  :: order
  complex(dp),      intent(out) :: A

  call get_amplitude_rcl (npr,pow,order,colour,hel,A)

  end subroutine wrapper_get_amplitude_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_get_polarized_squared_amplitude_rcl (npr,pow,order,hel,legs,A2h)

  integer,          intent(in)  :: npr,pow,legs
  integer,          intent(in)  :: hel(1:legs)
  character(len=*), intent(in)  :: order
  real(dp),         intent(out) :: A2h

  call get_polarized_squared_amplitude_rcl (npr,pow,order,hel,A2h)

  end subroutine wrapper_get_polarized_squared_amplitude_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_compute_colour_correlation_rcl (npr,p,legs,i1,i2, &
                                                     A2cc,momenta_check)

  integer,  intent(in)  :: npr,legs,i1,i2
  real(dp), intent(in)  :: p(0:3,1:legs)
  real(dp), intent(out) :: A2cc
  logical,  intent(out) :: momenta_check

  call compute_colour_correlation_rcl(npr,p,i1,i2,A2cc,momenta_check)

  end subroutine wrapper_compute_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_compute_all_colour_correlations_rcl(npr,p,legs, &
                                                         momenta_check)

  integer,  intent(in)  :: npr,legs
  real(dp), intent(in)  :: p(0:3,1:legs)
  logical,  intent(out) :: momenta_check

  call compute_all_colour_correlations_rcl(npr,p,momenta_check)

  end subroutine wrapper_compute_all_colour_correlations_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_rescale_colour_correlation_rcl (npr,i1,i2,A2cc)

  integer,  intent(in)  :: npr,i1,i2
  real(dp), intent(out) :: A2cc

  call rescale_colour_correlation_rcl(npr,i1,i2,A2cc)

  end subroutine wrapper_rescale_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_compute_spin_colour_correlation_rcl (npr,p,legs, &
                                                          i1,i2,v, &
                                                          A2scc, &
                                                          momenta_check)

  integer,     intent(in)  :: npr,legs,i1,i2
  real(dp),    intent(in)  :: p(0:3,1:legs)
  complex(dp), intent(in)  :: v(0:3)
  real(dp),    intent(out) :: A2scc
  logical,     intent(out) :: momenta_check

  call compute_spin_colour_correlation_rcl(npr,p,i1,i2,v,A2scc,momenta_check)
  
  end subroutine wrapper_compute_spin_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_rescale_spin_colour_correlation_rcl (npr,i1,i2,v,A2scc)

  integer,     intent(in)  :: npr,i1,i2
  complex(dp), intent(in)  :: v(0:3)
  real(dp),    intent(out) :: A2scc

  call rescale_spin_colour_correlation_rcl(npr,i1,i2,v,A2scc)
  
  end subroutine wrapper_rescale_spin_colour_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_compute_spin_correlation_rcl (npr,p,legs,j,v,A2sc, &
                                                   momenta_check)

  integer,     intent(in)  :: npr,legs,j
  real(dp),    intent(in)  :: p(0:3,1:legs)
  complex(dp), intent(in)  :: v(0:3)
  real(dp),    intent(out) :: A2sc
  logical,     intent(out) :: momenta_check

  call compute_spin_correlation_rcl (npr,p,j,v,A2sc,momenta_check)
  
  end subroutine wrapper_compute_spin_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_rescale_spin_correlation_rcl (npr,j,v,A2sc)

  integer,     intent(in)  :: npr,j
  complex(dp), intent(in)  :: v(0:3)
  real(dp),    intent(out) :: A2sc

  call rescale_spin_correlation_rcl (npr,j,v,A2sc)
  
  end subroutine wrapper_rescale_spin_correlation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine get_legs_rcl (npr,legs)

  ! This subroutine extracts the number of legs of the process with 
  ! process number "npr"

  integer, intent(in)  :: npr
  integer, intent(out) :: legs

  integer :: pr,i

  if (.not.processes_generated) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR: Call of get_legs_rcl not allowed:'
      write(nx,*) '       Processes not generated yet.'
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings.le.warning_limit) then
      warnings = warnings + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR: get_legs_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      call toomanywarnings
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  end subroutine get_legs_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine wrapper_get_momenta_rcl (npr,p,legs)

  integer,  intent(in)  :: npr,legs
  real(dp), intent(out) :: p(0:3,1:legs)

  call get_momenta_rcl (npr,p)

  end subroutine wrapper_get_momenta_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  end module wrapper_rcl

!#####################################################################

